//
//  MasterYodaSpeaksTests.swift
//  MasterYodaSpeaksTests
//
//  Created by Kemar White on 8/19/15.
//  Copyright (c) 2015 toohotz. All rights reserved.
//

import UIKit
import XCTest

class MasterYodaSpeaksTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testEndpointResponsePerformance() {
        // Testing API response time against a reasonable average of 3.5s
        self.measureBlock() {
            self.testEndpoint()
        }
    }

    func testEndpoint()
    {
        let testString = "Please master, help me"
        let networkManager = MYNetworkingManager()
       XCTAssertNotNil(networkManager.baseURL, "The base URL is nil")

        let endpointExpectation = self.expectationWithDescription("Endpoint works")
        networkManager.getDataFromEndPoint(testString, StringData: { (receivedData) -> () in
            XCTAssertNotNil(receivedData, "The received data is nil")
            XCTAssertNotEqual(receivedData as! String, NetworkConstants.applicationError, "An application error has occurred")
            0
        endpointExpectation.fulfill()
        })

        self.waitForExpectationsWithTimeout(10, handler: { (error) -> Void in
            if error == nil {
                println("The endpoint is working")
            }
        })
    }
}
