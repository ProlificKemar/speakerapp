//
//  SpeakerSelectionManager.swift
//  MasterYodaSpeaks
//
//  Created by Kemar White on 8/20/15.
//  Copyright (c) 2015 toohotz. All rights reserved.
//

import Foundation

class SpeakerSelectionManager {
   internal var plistPath: String!
    internal var isLocal: Bool
    var speakers: NSArray? {
        get {
            return listOfSpeakers()
        }
    }

    //MARK: Initializers

    /**
    Initializes the manager with a .plist that contains a list of selectable speakers.

    :param: localPathToPlist File path location to the .plist.
    :param: pathToPlist     Boolean that says true if the .plist is local to the device or false if the plist is located remotely.

    :returns: Returns a fully initialized speaker manager loaded with a list of speakers.
    */
    required init(pathToPlist: String!, isLocalPlist: Bool)
    {
        self.plistPath = pathToPlist
        self.isLocal = isLocalPlist
    }

    //MARK: Private Methods

    /**
    Returns a list of the current speakers that can be used for Language Processing.

    :returns: Array of available speakers.
    */
    private func listOfSpeakers() -> NSArray?
    {
        var listOfSpeakers:NSArray?
        if (isLocal) {
            listOfSpeakers = NSArray(contentsOfFile: NSBundle.mainBundle().bundlePath.stringByAppendingPathComponent(plistPath))!
        } else {
            // Need to test how performant this is with large .plist(s)
            listOfSpeakers = NSArray(contentsOfURL: NSURL(string: plistPath)!)!
        }
        return listOfSpeakers
    }
}