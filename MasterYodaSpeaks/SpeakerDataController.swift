//
//  SpeakerDataController.swift
//  MasterYodaSpeaks
//
//  Created by Kemar White on 8/20/15.
//  Copyright (c) 2015 toohotz. All rights reserved.
//

import Foundation
import UIKit

enum Speakers: String
{
    case Yoda = "Yoda"
    case Tester = "Tester"
}

protocol SpeakerDataControllerDelegate {

    func speakerDataControllerDelegateDidInputText(text: String!)
}

class SpeakerDataController: NSObject, UITextFieldDelegate {

    //MARK: Vars

    var speakerName: String!

    var inputTextField: UITextField!

    var delegate: SpeakerDataControllerDelegate!

    //MARK: Public Methods

    func setupWithDelegate(delegate: SpeakerDataControllerDelegate!, speaker: String!, inputTextField: UITextField)
    {
        self.delegate = delegate
        self.speakerName = speaker
        self.inputTextField = inputTextField
        self.inputTextField.delegate = self
    }

    //MARK: UITextFieldDelegate Methods

    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        if (textField.text as NSString).length >= 3 {
            delegate.speakerDataControllerDelegateDidInputText(textField.text)
        }

        return true
    }
}