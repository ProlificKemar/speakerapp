//
//  SpeakerViewController.swift
//  MasterYodaSpeaks
//
//  Created by Kemar White on 8/20/15.
//  Copyright (c) 2015 toohotz. All rights reserved.
//

import UIKit

class SpeakerViewController: UIViewController, SpeakerDataControllerDelegate {

    //MARK: vars

    var speaker: String!

    var networkingManager: MYNetworkingManager!

    let dataController = SpeakerDataController()

    var chatHistoryDataController: SpeakerChatHistoryDataController!

    var imageOriginalSize: CGRect!

    let iconDimension = 120

    //MARK: IBOutlets

    @IBOutlet weak var speakerDetailsLabel: UILabel!

    @IBOutlet weak var speakerTitleLabel: UILabel!

    @IBOutlet weak var speakerOutputTextView: UITextView!

    @IBOutlet weak var speakersHistoryLabel: UILabel!
    
    @IBOutlet weak var chatHistoryTableView: UITableView!

    @IBOutlet weak var inputTextField: UITextField!

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var masterVisualEffectView: UIVisualEffectView!

    @IBOutlet weak var speakerVisualEffectView: UIVisualEffectView!

     @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()

        dataController.setupWithDelegate(self, speaker: "Yoda", inputTextField: self.inputTextField)
        initializing()
        setupUI()
    }

    //MARK: Private Methods
    
    private func initializing()
    {
        networkingManager = MYNetworkingManager()
        let history = retrieveChatHistory()
        if history == nil {
            tableViewHeightConstraint.constant = 0
            self.view.updateConstraintsIfNeeded()
            self.view.needsUpdateConstraints()
        }

        // Keyboard observers

        chatHistoryDataController = SpeakerChatHistoryDataController(tableView: chatHistoryTableView, chatHistory: history)
    }

    private func setupUI()
    {
        self.navigationController?.navigationBarHidden = false
        self.title = self.speaker

        setupDefaultSpeakerLabels()

        // Create rounded imageview and setup its constraints

        let backgroundImage = UIImage(named: "yoda_wallpaper")?.resizableImageWithCapInsets(UIEdgeInsetsMake(100, 100, 100, 100))
        imageOriginalSize = speakerVisualEffectView.frame
        speakerVisualEffectView.backgroundColor = UIColor(patternImage: backgroundImage!.blurredImageWithRadius(50, iterations: 10, tintColor: nil))
        var yodaImageView = UIImageView(image: UIImage(named: "yoda_saber"))
        yodaImageView.setTranslatesAutoresizingMaskIntoConstraints(false)

        masterVisualEffectView.addSubview(yodaImageView)

        let heightConstraint   = NSLayoutConstraint(item: yodaImageView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: CGFloat(iconDimension) )

        let widthConstraint    = NSLayoutConstraint(item: yodaImageView, attribute: .Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: CGFloat(iconDimension) )

        let verticalConstraint = NSLayoutConstraint(item: yodaImageView, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: speakerDetailsLabel, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: -10)

        let centerXConstraint  = NSLayoutConstraint(item: yodaImageView.superview!, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: yodaImageView, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0)

        yodaImageView.addConstraints([heightConstraint, widthConstraint])
        yodaImageView.superview!.addConstraints([centerXConstraint, verticalConstraint])
        yodaImageView.layer.cornerRadius =  CGFloat(iconDimension / 2)
        yodaImageView.clipsToBounds = true

        setupSpeakerLabelsFromAPI()
    }

    private func setupDefaultSpeakerLabels()
    {
        speakerDetailsLabel.text   = "\(speakerDetailsLabel.text!)\(speaker)"
        speakerTitleLabel.text     = "\(speakerTitleLabel.text!) \(speaker)"
        speakerOutputTextView.text = "\(speaker)'s \(speakerOutputTextView.text!)"
        speakerOutputTextView.textColor = MYColors.yodaColor
        speakerOutputTextView.textAlignment = .Center
        speakersHistoryLabel.text  = (retrieveChatHistory() != nil) ? "\(speaker)'s last words" : nil
    }

    private func setupSpeakerLabelsFromAPI()
    {
        // Would be nice to have a single batch call to the API instead of individual API requests like below
        // API takes a noticeable amount of time to return the translated text so should load this method before view shows

        networkingManager.getDataFromEndPoint(self.speakerDetailsLabel.text, StringData: { (receivedData) -> () in

            // Need to check here if data being received is not an error and is a string
            if let stringData:AnyObject = receivedData where (receivedData as? String != nil) {
                self.speakerDetailsLabel.text = stringData as? String
            }
        })
        networkingManager.getDataFromEndPoint(self.speakerTitleLabel.text, StringData: { (receivedData) -> () in
            if let stringData:AnyObject = receivedData where (receivedData as? String != nil) {
                self.speakerTitleLabel.text = stringData as? String
            }
        })
        if retrieveChatHistory() != nil {
            networkingManager.getDataFromEndPoint(self.speakersHistoryLabel.text, StringData: { (receivedData) -> () in
                if let stringData:AnyObject = receivedData where (receivedData as? String != nil) {
                    self.speakersHistoryLabel.text = stringData as? String
                }
            })
        }
    }

    private func chatHistoryPath() -> String
    {
        let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentDir: NSString = (paths as NSArray).objectAtIndex(0) as! NSString
        return documentDir.stringByAppendingPathComponent("chatHistory.plist")
    }

    private func storeChatHistory(inputText: String, outputText: String)
    {
        let fileManager = NSFileManager.defaultManager()
        var path = chatHistoryPath()
        if (!fileManager.fileExistsAtPath(chatHistoryPath())) {

            let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
            let documentDir: NSString = (paths as NSArray).objectAtIndex(0) as! NSString
            path = documentDir.stringByAppendingPathComponent("chatHistory.plist")
        }

        var chatData: NSMutableDictionary?

        if fileManager.fileExistsAtPath(path) {
            chatData = NSMutableDictionary(contentsOfFile: path)
        } else {
            // File doesn't exist yet then empty dict
            chatData = NSMutableDictionary()
        }

        // Insert chat history and write it to plist
        chatData?.setObject(inputText, forKey: outputText)
        chatData?.writeToFile(path, atomically: true)
    }

    private func retrieveChatHistory() -> NSMutableDictionary?
    {
        return NSMutableDictionary(contentsOfFile: chatHistoryPath())
    }

    private func keybaordWasShown(notification: NSNotification)
    {
        let keyboardSize: CGSize! = (notification.userInfo as NSDictionary?)!.objectForKey(UIKeyboardFrameBeginUserInfoKey)?.CGRectValue().size

        // Adjust bottom inset of scroll view by the height of the keyboard on screen
        let contentInsets = UIEdgeInsetsMake(0, 0, keyboardSize.height, 0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }

    private func keyboardWillHide(notification: NSNotification)
    {
        scrollView.contentInset = UIEdgeInsetsZero
        scrollView.scrollIndicatorInsets = UIEdgeInsetsZero
    }

    //MARK: SpeakerDataControllerDelegate Methods

    func speakerDataControllerDelegateDidInputText(text: String!)
    {
        networkingManager.getDataFromEndPoint(text, StringData: { (receivedData) -> () in
            if let stringData:AnyObject = receivedData where (receivedData as? String != nil) {
            self.speakerOutputTextView.text = stringData as? String
            self.storeChatHistory(self.inputTextField.text, outputText: self.speakerOutputTextView.text)
            }
        })
    }
}

