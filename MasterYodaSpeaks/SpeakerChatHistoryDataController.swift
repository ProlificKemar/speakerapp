//
//  SpeakerChatHistoryDataController.swift
//  MasterYodaSpeaks
//
//  Created by Kemar White on 8/23/15.
//  Copyright (c) 2015 toohotz. All rights reserved.
//

import Foundation
import UIKit

protocol SpeakerChatHistoryDataControllerDelegate {

    func speakerChatHistoryDataControllerDidLoadTableView(loadedTableView: UITableView!)
}

class SpeakerChatHistoryDataController: NSObject, UITableViewDataSource, UITableViewDelegate {

    //MARK: Vars

    let tableView: UITableView

    let chatHistory: NSMutableDictionary?

    //MARK: Initializers
    
    /**
    Initializes the chat history data controller with a tableview.

    :param: tableView The tableview to initialize the data controller with.

    :returns: An initialized data controller with a tableview.
    */
    init(tableView: UITableView, chatHistory: NSMutableDictionary?) {
        self.tableView = tableView
        self.chatHistory = chatHistory
        super.init()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.reloadData()
    }

    //MARK: Private Methods

    private func cellForIndexPath(indexPath: NSIndexPath) -> UITableViewCell
    {
        var cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "Cell")
        cell.textLabel?.font = UIFont(name: "Avenir Next", size: 17)
        cell.detailTextLabel?.font = UIFont(name: "Avenir Next", size: 14)

        if chatHistory != nil {
            cell.textLabel?.text = (chatHistory!.allKeys as NSArray)[indexPath.row] as? String
            cell.textLabel?.numberOfLines = 0
            cell.detailTextLabel?.text = (chatHistory!.allValues as NSArray)[indexPath.row] as? String
            cell.textLabel?.textColor = MYColors.yodaColor
        }
        return cell
    }

    //MARK: UITableViewDelegate Method


    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {

        return (chatHistory != nil) ? chatHistory!.count : 0
    }

    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        return cellForIndexPath(indexPath)
    }
}