//
//  MYNetworkingManager.swift
//  MasterYodaSpeaks
//
//  Created by Kemar White on 8/22/15.
//  Copyright (c) 2015 toohotz. All rights reserved.
//

import Foundation
import HTZNetworkingManager
import Alamofire

struct NetworkConstants {
    static let applicationError = "Application Error"
}

class MYNetworkingManager: HTZNetworkingManager {

    let headers = ["X-Mashape-Key": "HnVUBm36w2msh8Kk7YJ6L5bibfRVp1yexlTjsn51NXs4u8R32S", "Accept": "text/plain"]

    let baseURL: String? = "https://yoda.p.mashape.com/yoda?sentence="

    required init()
    {
        super.init(baseURL: self.baseURL)
    }

    required init(configuration: NSURLSessionConfiguration?, serverTrustPolicyManager: ServerTrustPolicyManager?) {
        fatalError("init(configuration:serverTrustPolicyManager:) has not been implemented")
    }

    func getDataFromEndPoint(endpoint: String?, StringData: (receivedData: AnyObject?) -> ()) {
        if let url = endpoint {
            let requestURL = "\(baseURL!)\(formatEndpoint(url))"
            Alamofire.request(.GET, requestURL, headers: headers).responseString(encoding: NSUTF8StringEncoding, completionHandler: { (_, _, responseString, error) -> Void in
                // Need to check for possible application error


                var appErrorString = NSString()
                if (appErrorString.rangeOfString("Application Error").location != NSNotFound) {
                    println("An application error has occurred")
                }
                if (error != nil) {
                    StringData(receivedData: error)
                } else if (responseString != nil) {
                    if let applicationErrorString = (responseString as NSString?) {

                        // Check to make sure an application error has not occured from the API
                        if (applicationErrorString.rangeOfString("Application Error").location != NSNotFound) {

                            // Create an NSError object with applicaiton error detail
                            let error = NSError(domain: NetworkConstants.applicationError, code: 1, userInfo: [NSLocalizedDescriptionKey : "An application error has occurred from the API"])
                            StringData(receivedData: error)
                        return
                        }
                    }
                    StringData(receivedData: responseString)
                }
            })
        } else {
            print("An error occurred trying to parse the endpoint tha you provided.", appendNewline: true)
        }
    }

    private func formatEndpoint(endpoint: NSString) -> String
    {
        return endpoint.stringByReplacingOccurrencesOfString(" ", withString: "+") as String
    }
}