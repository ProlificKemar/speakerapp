//
//  GlobalConstants.swift
//  MasterYodaSpeaks
//
//  Created by Kemar White on 8/24/15.
//  Copyright (c) 2015 toohotz. All rights reserved.
//

import Foundation
import UIKit

public struct MYColors {
    static let yodaColor = UIColor(red: 138/255, green: 163/255, blue: 112/255, alpha: 0.725)
}
