//
//  ViewController.swift
//  MasterYodaSpeaks
//
//  Created by Kemar White on 8/19/15.
//  Copyright (c) 2015 toohotz. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    //MARK: IBOutlets

    @IBOutlet weak var speakerPickerView: UIPickerView!

    //MARK: Vars

    let plistPath = "Speakers.plist"

    var speakerSelectionManager: SpeakerSelectionManager!

    //MARK: View Methods
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadData()
    }

    //MARK: Private Methods

    private func setupUI()
    {
        let backButtonImage = UIImage(named: "btn_back")?.resizableImageWithCapInsets(UIEdgeInsetsMake(0, 2, 0, 0))
        var backButton = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        backButton.setBackButtonBackgroundImage(backButtonImage, forState: .Normal, barMetrics: .Default)
        self.navigationItem.backBarButtonItem = backButton
    }

    private func loadData()
    {
        self.speakerSelectionManager = SpeakerSelectionManager(pathToPlist:plistPath , isLocalPlist: true)
    }

    //MARK: UIPickerViewDelegate Methods

    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String!
    {
        return self.speakerSelectionManager.speakers!.objectAtIndex(row) as! String
    }

   func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int
    {
        return 1
    }

    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return self.speakerSelectionManager.speakers!.count
    }

    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView
    {
        var labelView = view as? UILabel

        if labelView == nil {
            labelView = UILabel()
            labelView?.font = UIFont(name: "Avenir Next", size: 17)
            labelView?.textAlignment = .Center
            labelView?.numberOfLines = 2
        }
        labelView?.text = self.speakerSelectionManager.speakers![row] as? String

        return labelView!
    }

    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        var selectedSpeaker = Speakers(rawValue: self.speakerSelectionManager.speakers!.objectAtIndex(row) as! String)

        if selectedSpeaker != nil {
            var destinationVC = self.storyboard?.instantiateViewControllerWithIdentifier("speakerViewController") as! SpeakerViewController
            destinationVC.speaker = selectedSpeaker?.rawValue
            self.navigationController?.pushViewController(destinationVC, animated: true)
        }
    }
}

