//
//  IQKeyboardManagerSwift.h
//  IQKeyboardManagerSwift
//
//  Created by Kemar White on 8/25/15.
//  Copyright (c) 2015 toohotz. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for IQKeyboardManagerSwift.
FOUNDATION_EXPORT double IQKeyboardManagerSwiftVersionNumber;

//! Project version string for IQKeyboardManagerSwift.
FOUNDATION_EXPORT const unsigned char IQKeyboardManagerSwiftVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <IQKeyboardManagerSwift/PublicHeader.h>


